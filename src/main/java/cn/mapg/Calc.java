package cn.mapg;

/**
 * 计算接口
 * @author AS
 *
 */
public interface Calc {
	/**
	 * 运算过程
	 * @param num1 运算数1	
	 * @param num2 运算数2
	 * @return 运算结果
	 */
	public Double compute(double num1, double num2);
}
