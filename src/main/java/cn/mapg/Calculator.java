package cn.mapg;

import java.util.Map;

import cn.mapg.config.OperationContext;

public class Calculator {
	
	/** map集合用于存放Calc对象*/
	private Map<String,Calc> m = null;
	
	/**
	 * 给map初始化
	 * @param oc 
	 */
	public Calculator(OperationContext oc)
	{
		m = oc.getBeans();
	}

	/**
	 * 根据key找对应的的value(Calc类型的运算对象),调用运算对象的compute方法,传入参数,返回计算结果
	 * @param k map中的k值
	 * @param num1 运算数1
	 * @param num2 运算数2
	 * @return 计算结果
	 */
	public double run(String k,double num1, double num2)
	{
		Calc c = m.get(k);
		return c.compute(num1, num2);
	}
	
}
