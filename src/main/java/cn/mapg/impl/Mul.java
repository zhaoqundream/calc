package cn.mapg.impl;

import cn.mapg.Calc;

public class Mul implements Calc
{
	public Double compute(double num1, double num2)
	{
		return num1*num2;
		
	}
	
}
