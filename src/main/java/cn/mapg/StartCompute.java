package cn.mapg;

import java.util.Scanner;

import cn.mapg.config.OperationContext;

public class StartCompute 
{
	/** 计算类型*/
	private static String type;
	
	/** 解析文件对象*/
	private static OperationContext oc = null;
	
	/** 计算器对象*/
	private static Calculator c = null;
	
	/** 输入对象*/
	private static Scanner scan = null;
	
	public static void main(String[] args) {
		try {
			oc = new OperationContext("context.xml");
			c = new Calculator(oc);
			scan = new Scanner(System.in);
			System.out.println("请选择你要计算的类型(+、-、*、/)中的一个(输入'-1'退出):");
			while(true)
			{
				type = scan.nextLine();
				
				if("+".equals(type))
				{
					System.out.println("请输入两个计算数:");
					
					/** 加计算*/
					System.out.println("结果为:"+c.run("add", scan.nextDouble(),
												scan.nextDouble()));
					System.out.println("请选择你要计算的类型(+、-、*、/)中的一个(输入'-1'退出):");
				}
				else if("-".equals(type))
				{
					System.out.println("请输入两个计算数:");
					
					/** 减计算*/
					System.out.println("结果为:"+c.run("minus", scan.nextDouble(),
												scan.nextDouble()));
					System.out.println("请选择你要计算的类型(+、-、*、/)中的一个(输入'-1'退出):");
				}
				else if("*".equals(type))
				{
					System.out.println("请输入两个计算数:");
					
					/** 乘计算*/
					System.out.println("结果为:"+c.run("mul", scan.nextDouble(),
												scan.nextDouble()));
					System.out.println("请选择你要计算的类型(+、-、*、/)中的一个(输入'-1'退出):");
				}
				else if("/".equals(type))
				{
					System.out.println("请输入两个计算数:");
					
					/** 除计算*/
					System.out.println("结果为:"+c.run("div", scan.nextDouble(),
												scan.nextDouble()));
					System.out.println("请选择你要计算的类型(+、-、*、/)中的一个(输入'-1'退出):");
				}
				else if("-1".equals(type))
				{
					break;
				}
			}
			System.out.println("已退出计算!!!");
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
